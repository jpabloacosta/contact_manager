from celery import Celery
from celery.schedules import crontab


def start(app, config):
    app.config['CELERY_BROKER_URL'] = config.get('celery_broker_url')
    app.config['CELERY_RESULT_BACKEND'] = config.get('celery_broker_url')

    app.config['CELERY_IMPORTS'] = (
        'tasks.create_contact', 'tasks.remove_contact'
    )

    app.config['CELERYBEAT_SCHEDULE'] = {
        'periodic_task_create_contact': {
            'task': 'tasks.create_contact.create',
            'schedule': config.get('create_contact_period', 60)
        }
    }

    celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return celery.Task.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery
