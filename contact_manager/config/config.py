import os
import json
import connexion
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from . import celery_config

# configuration file in json format
CONFIG_FILE = "config.json"

# path of parent folder
basedir = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

config = dict()
with open(os.path.join(basedir, "config", CONFIG_FILE)) as f:
    config = json.load(f)

# absolute database path
db_abs_path = os.path.join(basedir, config.get('rel_database_path'))

connex_app = connexion.App(
    __name__, specification_dir=os.path.join(basedir, "config"))
app = connex_app.app

# SQLALCHEMY configuration
app.config['SQLALCHEMY_ECHO'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////' + db_abs_path
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

# configure celery
celery = celery_config.start(app, config)
