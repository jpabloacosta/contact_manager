from flask import make_response, abort
from config.config import db
from models.email import Email, EmailSchema


def get_all(contact_id):
    """Get all the email associated to the contact id.

    :Parameters:
        - `contact_id`: contact id associated to the email.
    :return:    list of email
    """
    emails = (
        Email.query.filter(Email.contact_id == contact_id)
        .order_by(Email.timestamp).all())

    schema = EmailSchema(many=True)
    data = schema.dump(emails)
    return data


def get_one(contact_id, email_id):
    """Get the email using the email's id.

    :Parameters:
        - `contact_id`: contact id associated to the email.
        - `email_id`: id of the email to obtain.
    :return:    email
    """
    email = (
        Email.query
        .filter(Email.contact_id == contact_id)
        .filter(Email.email_id == email_id)
        .one_or_none()
    )
    if email is not None:
        schema = EmailSchema()
        data = schema.dump(email)
        return data
    else:
        abort(
            404,
            "Contact email with id {email_id} not found"
            .format(email_id=email_id),
        )


def create(contact_id, email):
    """Creates a email associated to the provided contact id, using the
    information provided

    :Parameters:
        - `contact_id`: contact id associated to the email.
        - `email`: email information.
    :return:    email recently created
    """
    address = email.get("address")

    address_exists = (
        Email.query.filter(Email.address == address)
        .one_or_none()
    )

    if address_exists is None:
        email['contact_id'] = contact_id

        schema = EmailSchema()
        new_email = schema.load(email, session=db.session)
        db.session.add(new_email)
        db.session.commit()

        data = schema.dump(new_email)

        return data, 201
    else:
        abort(
            409,
            "Email {address} exists already".format(address=address),
        )


def update(contact_id, email_id, email):
    """Updates a given email associated to the provided contact id, using the
    information provided

    :Parameters:
        - `contact_id`: contact id associated to the email.
        - `email_id`: email id to remove.
        - `email`: information for updating.
    :return:    email recently updated
    """
    current_email = (
        Email.query
        .filter(Email.contact_id == contact_id)
        .filter(Email.email_id == email_id)
        .one_or_none()
    )

    if current_email is not None:
        email['contact_id'] = contact_id

        schema = EmailSchema()
        update = schema.load(email, session=db.session)
        update.email_id = current_email.email_id

        db.session.merge(update)
        db.session.commit()

        data = schema.dump(current_email)
        return data, 200
    else:
        abort(
            404,
            "Contact email id {email_id} not found"
            .format(email_id=email_id)
        )


def delete(contact_id, email_id):
    """Deletes the given email associated to the provided contact id.

    :Parameters:
        - `contact_id`: contact id associated to the email.
        - `email_id`: email id to remove.
    :return:    200 if sucessful deleted, 404 if contact id does not exist
    """
    email = email = (
        Email.query
        .filter(Email.contact_id == contact_id)
        .filter(Email.email_id == email_id)
        .one_or_none()
    )

    if email is not None:
        db.session.delete(email)
        db.session.commit()
        return make_response(
            "Contact email {email_id} successfully deleted"
            .format(email_id=email_id),
            200
            )
    else:
        abort(
            404,
            "Contact email {email_id} not found"
            .format(email_id=email_id)
        )
