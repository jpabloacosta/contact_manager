from flask import make_response, abort
from config.config import db
from models.contact import Contact, ContactSchema
from models.email import Email, EmailSchema
from tasks import remove_contact


def get_all():
    """Get all contacts stored in the database.

    :return:    List of contacts
    """
    contacts = Contact.query.order_by(Contact.contact_id).all()
    contact_schema = ContactSchema(many=True)
    data = contact_schema.dump(contacts)
    return data


def get_one(username):
    """Get the contact with the given username.

    :Parameters:
        - `username`: contact's username
    :return:    contact
    """
    contact = Contact.query.filter(Contact.username == username).one_or_none()
    if contact is not None:
        contact_schema = ContactSchema()
        data = contact_schema.dump(contact)
        return data
    else:
        abort(
            404,
            "Contact with username {username} not found"
            .format(username=username),
        )


def create(contact):
    """Creates a new contact with the information provided.

    :Parameters:
        - `contact`: contact's information: username, first_name, etc.
    :return:    contact recently created
    """
    username = contact.get("username")

    username_exists = (
        Contact.query.filter(Contact.username == username)
        .one_or_none()
    )

    if username_exists is None:
        schema = ContactSchema()
        new_contact = schema.load(contact, session=db.session)
        db.session.add(new_contact)
        db.session.commit()
        data = schema.dump(new_contact)

        # Delete the contact after its expiration time
        remove_contact.remove.apply_async(
            args=[data['contact_id']], countdown=60)
        return data, 201
    else:
        abort(
            409,
            "Username {username} exists already".format(username=username),
        )


def update(contact_id, contact):
    """Updates a the given contact with the information provided.

    :Parameters:
        - `contact_id`: contact id to update.
        - `contact`: contact's information: username, first_name, etc.
    :return:    contact recently updated
    """
    current_contact = (
        Contact.query.filter(Contact.contact_id == contact_id).one_or_none()
    )

    if current_contact is not None:
        schema = ContactSchema()
        update = schema.load(contact, session=db.session)
        update.contact_id = current_contact.contact_id

        db.session.merge(update)
        db.session.commit()

        data = schema.dump(current_contact)
        return data, 200
    else:
        abort(
            404, "Contact {contact_id} not found".format(contact_id=contact_id)
        )


def delete(contact_id):
    """Deletes the given contact.

    :Parameters:
        - `contact_id`: contact id to remove.
    :return:    200 if sucessful, 404 if contact id does not exist
    """
    contact = (
        Contact.query.filter(Contact.contact_id == contact_id).one_or_none()
    )

    if contact is not None:
        db.session.delete(contact)
        db.session.commit()
        return make_response(
            "Contact {username} successfully deleted"
            .format(username=contact.username),
            200)
    else:
        abort(
            404, "Contact {contact_id} not found".format(contact_id=contact_id)
        )
