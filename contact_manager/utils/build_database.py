import os
from config.config import db, db_abs_path
from models.contact import Contact
from models.email import Email


CONTACTS = [
    {
        "username": "pacosta",
        "first_name": "Pablo",
        "last_name": "Acosta",
    },
    {
        "username": "jcowan",
        "first_name": "James",
        "last_name": "Cowan",
    },
    {
        "username": "pbenet",
        "first_name": "Peter",
        "last_name": "Benet",
    }
]

CONTACT_EMAILS = [
    {
        "contact_id": "1",
        "address": "jpablo@acostapadron.es",
    },
    {
        "contact_id": "1",
        "address": "jpablo@acostapadron.com",
    },
    {
        "contact_id": "3",
        "address": "pbenet@hotmail.com",
    }
]


def build_db():
    # if the database exists, deletes it for rebuilding it
    if os.path.exists(db_abs_path):
        os.remove(db_abs_path)

    db.create_all()

    # add the contacts to the database
    for contact in CONTACTS:
        c = Contact(
            username=contact.get("username"),
            first_name=contact.get("first_name"),
            last_name=contact.get("last_name"))
        db.session.add(c)

    # add the emails to the database
    for email in CONTACT_EMAILS:
        ce = Email(
            contact_id=email.get("contact_id"),
            address=email.get("address"))
        db.session.add(ce)

    db.session.commit()

if __name__ == '__main__':
    build_db()
