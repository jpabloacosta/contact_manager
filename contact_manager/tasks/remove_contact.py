import celery
import requests

BASE_URL = "http://localhost:5000/contacts"


@celery.task()
def remove(contact_id):
    """Celery task for removing a contact and the associated emails.

    :Parameters:
        - `contact_id`: contact id associated to the emails
    """
    logger = remove.get_logger()
    contact_id = str(contact_id)
    contact_url = BASE_URL + "/" + str(contact_id)

    logger.info("Removing contact: %s" % contact_id)
    if _remove_emails(contact_id):
        logger.info("Contact's emails removed: %s" % contact_id)
    else:
        logger.warning("Error removing contact's emails: %s" % contact_id)

    r = requests.delete(url=contact_url)
    if r.status_code == 200:
        logger.info("Contact: %s removed" % contact_id)
    else:
        logger.warning("Error removing contact: %s" % contact_id)


def _remove_emails(contact_id):
    """Removes all emails associated to de given contact id.

    :Parameters:
        - `contact_id`: contact id associated to the emails
    :return:    True if successfull
    """
    emails_url = "/".join([BASE_URL, contact_id, "emails"])
    error = False
    resp = requests.get(url=emails_url)
    if resp.status_code == 200:
        emails = resp.json()
        for email in emails:
            email_id = str(email['email_id'])
            remove_email_url = emails_url + "/" + email_id
            delete_resp = requests.delete(url=remove_email_url)
            if delete_resp.status_code != 200:
                error = True

    return resp.status_code == 200 and not error
