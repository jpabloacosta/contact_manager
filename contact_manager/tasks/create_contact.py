import celery
import requests
import uuid
from random import randrange

BASE_URL = "http://localhost:5000/contacts"
MAX_NUM_EMAILS = 5


@celery.task()
def create():
    """Celery task for creating a contact using random data.
    """
    logger = create.get_logger()

    contact = {
        'username': uuid.uuid4().hex[:11],
        'first_name': uuid.uuid4().hex[:15],
        'last_name': uuid.uuid4().hex[:15]
    }

    logger.info("Creating contact: %s" % contact)
    r = requests.post(url=BASE_URL, json=contact)
    data = r.json()
    if r.status_code == 201:
        contact_id = str(data['contact_id'])
        logger.info("Contact: %s created" % data)
        logger.info("Creating contact's emails for contact: %s" % contact_id)

        if _create_emails(contact_id, MAX_NUM_EMAILS):
            logger.info("Emails created for contact: %s" % contact_id)
        else:
            logger.warning("Error creating emails for: %s" % contact_id)
    else:
        logger.warning("Error creating contact: %s" % contact)


def _create_emails(contact_id, max_num_emails):
    """Creates a random number of emails for the given contact using random data.

    :Parameters:
        - `contact_id`: contact id associated to the emails
        - `max_num_emails`: maximum number of emails
    :return:    True if successfull
    """
    emails_url = "/".join([BASE_URL, contact_id, "emails"])
    error = False
    for _ in range(randrange(MAX_NUM_EMAILS)):
        email = {
            'address': uuid.uuid4().hex[:11] + "@email" + ".com"
        }
        resp = requests.post(url=emails_url, json=email)
        if resp.status_code != 201:
            error = True
    return not error
