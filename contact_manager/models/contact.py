from datetime import datetime
from marshmallow import fields
from config.config import db, ma
from models.email import Email


class Contact(db.Model):
    __tablename__ = "contact"
    contact_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15))
    first_name = db.Column(db.String(35))
    last_name = db.Column(db.String(35))

    emails = db.relationship(
        "Email",
        backref="contact",
        cascade="all, delete, delete-orphan",
        single_parent=True,
        order_by="desc(Email.timestamp)",
    )

    timestamp = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


class ContactSchema(ma.ModelSchema):
    class Meta:
        model = Contact
        sqla_session = db.session

    emails = fields.Nested("ContactEmailSchema", default=[], many=True)


class ContactEmailSchema(ma.ModelSchema):
    email_id = fields.Int()
    contact_id = fields.Int()
    address = fields.Str()
    timestamp = fields.Str()
