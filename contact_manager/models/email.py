from datetime import datetime
from config.config import db, ma


class Email(db.Model):
    __tablename__ = "email"
    email_id = db.Column(db.Integer, primary_key=True)
    contact_id = db.Column(db.Integer, db.ForeignKey("contact.contact_id"))
    address = db.Column(db.String(255))
    timestamp = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


class EmailSchema(ma.ModelSchema):
    class Meta:
        include_fk = True
        model = Email
        sqla_session = db.session
