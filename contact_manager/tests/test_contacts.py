import unittest
import requests
from utils import build_database


class TestContact(unittest.TestCase):
    BASE_URL = "http://localhost:5000/contacts"

    def test_get_all_contacts(self):
        url = self.BASE_URL

        # rebuild database
        build_database.build_db()

        r = requests.get(url=url)
        data = r.json()
        self.assertEqual(len(data), 3)

    def test_get_one_contact(self):
        url = "/".join([self.BASE_URL, "pacosta"])
        expected = {
            'contact_id': 1,
            'first_name': 'Pablo',
            'last_name': 'Acosta',
            'username': 'pacosta'}

        # rebuild database
        build_database.build_db()

        r = requests.get(url=url)
        data = r.json()

        for key in expected:
            self.assertEqual(data.get(key), expected[key])

    def test_create_contact(self):
        url = self.BASE_URL
        new_contact = {
            'first_name': 'NewUserfirst_name',
            'last_name': 'NewUserlast_name',
            'username': 'NewUserUsername'}

        # rebuild database
        build_database.build_db()

        r = requests.post(url=url, json=new_contact)
        data = r.json()

        self.assertEqual(data['contact_id'], 4)
        for key in new_contact:
            self.assertEqual(data.get(key), new_contact[key])

    def test_update_contact(self):
        url = "/".join([self.BASE_URL, "2"])

        expected = {
            'contact_id': 2,
            'first_name': 'UpdatedUserfirst_name',
            'last_name': 'UpdatedUserlast_name',
            'username': 'UpdatedUser'}

        # rebuild database
        build_database.build_db()

        r = requests.put(url=url, json=expected)
        data = r.json()

        for key in expected:
            self.assertEqual(data.get(key), expected[key])

    def test_remove_contact(self):
        url = "/".join([self.BASE_URL, "3"])

        # rebuild database
        build_database.build_db()

        r = requests.delete(url=url)
        self.assertEqual(r.status_code, 200)

        r = requests.delete(url=url)
        self.assertEqual(r.status_code, 404)


if __name__ == '__main__':
    unittest.main()
