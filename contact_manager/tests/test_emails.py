import unittest
import requests
from utils import build_database


class TestEmail(unittest.TestCase):
    BASE_URL = "http://localhost:5000/contacts/1/emails"

    def test_get_all_emails(self):
        url = self.BASE_URL

        build_database.build_db()

        r = requests.get(url=url)
        data = r.json()
        self.assertEqual(len(data), 2)

    def test_get_one_email(self):
        url = "/".join([self.BASE_URL, "2"])
        expected = {
            'email_id': 2,
            'contact_id': 1,
            'address': "jpablo@acostapadron.com"
        }

        build_database.build_db()

        r = requests.get(url=url)
        data = r.json()

        for key in expected:
            self.assertEqual(data.get(key), expected[key])

    def test_create_email(self):
        url = self.BASE_URL

        new_email = {
            'address': "user2@hotmail.com"
        }

        # rebuild database
        build_database.build_db()

        r = requests.post(url=url, json=new_email)
        data = r.json()
        for key in new_email:
            self.assertEqual(data[key], new_email[key])

    def test_update_email(self):
        url = "/".join([self.BASE_URL, "2"])

        updated_email = {
            'address': "otheruser@hotmail.com"
        }

        expected = {
            'email_id': 2,
            'contact_id': 1,
            'address': "otheruser@hotmail.com"
        }

        # rebuild database
        build_database.build_db()

        r = requests.put(url=url, json=updated_email)
        data = r.json()
        for key in expected:
            self.assertEqual(data[key], expected[key])

    def test_remove_email(self):
        url = "/".join([self.BASE_URL, "1"])

        # rebuild database
        build_database.build_db()

        r = requests.delete(url=url)
        self.assertEqual(r.status_code, 200)

        # check that emails was removed and code 404 is received
        r = requests.delete(url=url)
        self.assertEqual(r.status_code, 404)


if __name__ == '__main__':
    unittest.main()
