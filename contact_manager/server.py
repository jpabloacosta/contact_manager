import connexion
import os
from config.config import connex_app, celery, db_abs_path
from utils import build_database


def start_server():
    if not os.path.exists(db_abs_path):
        build_database.build_db()

    connex_app.add_api("swagger.yml")
    connex_app.run(debug=True)


if __name__ == '__main__':
    start_server()
